package com.example.school.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @description:
 * @author: chenshijie
 * @create: 2022-09-21 19:07
 **/
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class CourseDTO {

    private Integer c_id;

    /**
     * 课程名称
     */
    private String c_name;

    /**
     * 教师ID
     */

    private Integer t_id;
}
