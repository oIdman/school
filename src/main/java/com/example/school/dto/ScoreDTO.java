package com.example.school.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @description:
 * @author: chenshijie
 * @create: 2022-09-21 19:07
 **/
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class ScoreDTO {
    /**
     * 学生ID
     */
    private Integer s_id;

    /**
     * 课程ID
     */
    private Integer c_id;

    /**
     * 分数
     */
    private Integer score;
}
