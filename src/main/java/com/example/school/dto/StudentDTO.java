package com.example.school.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * @description:
 * @author: chenshijie
 * @create: 2022-09-21 19:07
 **/
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class StudentDTO {
    private Integer id;

    private String name;

    private String gender;

    private Integer age;
}
