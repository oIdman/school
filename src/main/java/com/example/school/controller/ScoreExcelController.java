package com.example.school.controller;

import com.example.school.dto.ScoreExcelDTO;
import com.example.school.exception.BusinessException;
import com.example.school.service.bussiness.IScoreExcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-21
 */

@Controller
public class ScoreExcelController {
    @Autowired
    private IScoreExcelService scoreExcelService;

    /**
     * 输出成绩单（excel）
     *
     * @param scoreExcelDTO
     * @return
     */
    @GetMapping("/score/excel")
    public void excelForScore(HttpServletResponse httpServletResponse,  ScoreExcelDTO scoreExcelDTO) {

        ServletOutputStream outputStream;
        try {
            outputStream = httpServletResponse.getOutputStream();
        } catch (IOException e) {
            throw new BusinessException("获取输出流异常");
        }

        String fileName;
        try {
            fileName = URLEncoder.encode("成绩单.xlsx", StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new BusinessException("URL编码文件名出现异常");
        }

        httpServletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
        scoreExcelService.score(outputStream, scoreExcelDTO);
    }

}

