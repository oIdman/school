package com.example.school.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.school.dto.TeacherDTO;
import com.example.school.entity.Teacher;
import com.example.school.pojo.ResultJson;
import com.example.school.service.ITeacherService;
import com.example.school.vo.TeacherCountVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@RestController
public class TeacherController {
    @Autowired
    private ITeacherService teacherService;

    /**
     * 教师新增
     *
     * @param name
     * @return
     */
    @GetMapping ("/teacher/add")
    public ResultJson add(@RequestParam String name){
        teacherService.addTeacher(name);
        return ResultJson.success();
    }

    /**
     * 教师列表
     *
     * @param
     * @return
     */
    @GetMapping("/teacher/list")
    public TeacherCountVO list(){
        Wrapper<Teacher> queryWrapper = new QueryWrapper<>();
        List<Teacher> teacherList = teacherService.list(queryWrapper);
        Integer count = teacherService.count(queryWrapper);
        TeacherCountVO teacherCountVO = new TeacherCountVO(count,teacherList);
        return teacherCountVO;
    }

}

