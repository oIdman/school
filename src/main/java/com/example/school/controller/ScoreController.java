package com.example.school.controller;


import com.example.school.dto.ScoreDTO;
import com.example.school.entity.Score;
import com.example.school.pojo.ResultJson;
import com.example.school.service.IScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@RestController
public class ScoreController {
    @Autowired
    private IScoreService scoreService;

    /**
     * 分数列表
     *
     * @param
     * @return
     */
    @GetMapping("/score/list")
    public List<Score> list(){
        List<Score> scoreList = scoreService.scoreList();
        return scoreList;
    }

    /**
     * 分数新增
     *
     * @param scoreDTO
     * @return
     */
    @PostMapping("/score/add")
    public ResultJson add(@RequestBody ScoreDTO scoreDTO){
        return scoreService.scoreAdd(scoreDTO);
    }
}

