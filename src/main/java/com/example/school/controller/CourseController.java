package com.example.school.controller;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.school.entity.Course;
import com.example.school.pojo.ResultJson;
import com.example.school.service.ICourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@RestController
public class CourseController {
    @Autowired
    private ICourseService courseService;

    /**
     * 课程列表
     *
     * @param
     * @return
     */
    @GetMapping("/course/list")
    public List<Course> list(){
        Wrapper<Course> queryWrapper = new QueryWrapper<>();
        List<Course> courseList = courseService.list(queryWrapper);
        return courseList;
    }

    /**
     * 课程新增
     *
     * @param course
     * @return
     */
    @PostMapping("/course/add")
    public ResultJson add(@RequestBody Course course){

        return courseService.addCourse(course);
    }
}

