package com.example.school.controller;

import com.example.school.pojo.ResultJson;
import com.example.school.entity.Student;
import com.example.school.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */

@RestController
public class StudentController {
    @Autowired
    private IStudentService studentService;

    /**
     * 学生列表
     *
     * @param
     * @return
     */
    @GetMapping("/student/list")
    public List<Student> studentList(){
        return studentService.studentList();
    }

    /**
     * 学生新增
     *
     * @param student
     * @return
     */
    @PostMapping("/student/insert")
    public ResultJson add(@RequestBody Student student) {

        return studentService.add(student);
    }

    /**
     * 学生更新
     *
     * @param student
     * @return
     */
    @PostMapping("/student/update")
    public ResultJson update(@RequestBody Student student) {
        return studentService.update(student);
    }

    /**
     * 学生删除
     *
     * @param id
     * @return
     */
    @GetMapping("/student/delete")
    public ResultJson delete(@RequestParam Integer id){
        return studentService.delete(id);
    }
}
