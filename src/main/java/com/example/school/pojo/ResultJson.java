package com.example.school.pojo;


import com.example.school.emuns.CodeEnum;

public class ResultJson<T> {

        private Boolean state = true;
        private String message;
        private T result;
        private Integer code;

        public ResultJson() {
            this.code = CodeEnum.SUCCESS.getCode();
        }

        public ResultJson(String msg) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.message = msg;
        }

        public ResultJson(Boolean state, String message) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.message = message;
        }

        public ResultJson(boolean state, T result) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.result = result;
        }

        public ResultJson(CodeEnum codeenum) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.code = codeenum.getCode();
            this.message = codeenum.getDescription();
        }

        public ResultJson(boolean state, Integer code, T result) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.result = result;
            this.code = code;
        }

        public ResultJson(boolean state, Integer code, String message) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.message = message;
            this.code = code;
        }

        public ResultJson(boolean state, T result, String message) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.result = result;
            this.message = message;
        }

        public ResultJson(boolean state, Integer code, T result, String message) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.result = result;
            this.message = message;
            this.code = code;
        }

        public ResultJson(boolean state, int code, String description, T t) {
            this.code = CodeEnum.SUCCESS.getCode();
            this.state = state;
            this.result = t;
            this.message = description;
            this.code = code;
        }

        public static ResultJson fail(String message) {
            return new ResultJson(false, CodeEnum.FAIL.code, message);
        }

        public static ResultJson fail(Integer code, String message) {
            return new ResultJson(false, code, message);
        }

        public static ResultJson fail(CodeEnum codeEnum) {
            return new ResultJson(false, codeEnum.getCode(), codeEnum.getDescription());
        }

        public static ResultJson fail(Integer code, String message, Object t) {
            return new ResultJson(false, code, message, t);
        }

        public static ResultJson success() {
            return new ResultJson(true, CodeEnum.SUCCESS.code, CodeEnum.SUCCESS.description);
        }

        public static ResultJson success(Integer code, String message) {
            return new ResultJson(true, code, message);
        }

        public static ResultJson success(Integer code, String message, Object t) {
            return new ResultJson(true, code, message, t);
        }

        public static ResultJson success(String message) {
            return new ResultJson(true, CodeEnum.SUCCESS.code, message);
        }

        public static ResultJson success(Object t) {
            return new ResultJson(true, CodeEnum.SUCCESS.code, CodeEnum.SUCCESS.description, t);
        }

        public static ResultJson success(String message, Object t) {
            return new ResultJson(true, CodeEnum.SUCCESS.code, message, t);
        }

        public static ResultJson<Void> successWithVoid() {
            return new ResultJson(true, CodeEnum.SUCCESS.code, CodeEnum.SUCCESS.description);
        }

        public static <T> ResultJson<T> successWithResult(T result) {
            return new ResultJson(true, CodeEnum.SUCCESS.code, CodeEnum.SUCCESS.description, result);
        }

        public boolean isState() {
            return this.state;
        }

        public ResultJson setState(boolean state) {
            this.state = state;
            return this;
        }

        public String getMessage() {
            return this.message;
        }

        public ResultJson setMessage(String message) {
            this.message = message;
            return this;
        }

        public ResultJson setResult(T result) {
            this.result = result;
            return this;
        }

        public Integer getCode() {
            return this.code;
        }

        public ResultJson setCode(int code) {
            this.code = code;
            return this;
        }

        public Boolean getState() {
            return this.state;
        }

        public ResultJson setState(Boolean state) {
            this.state = state;
            return this;
        }

        public T getResult() {
            return this.result;
        }

        public void setCode(Integer code) {
            this.code = code;
        }
}


