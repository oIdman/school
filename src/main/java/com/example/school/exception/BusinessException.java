package com.example.school.exception;


import com.example.school.emuns.CodeEnum;

public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    protected Integer errorCode;
    protected String errorMsg;

    public BusinessException(CodeEnum codeEnum) {
        super(codeEnum.getDescription());
        this.errorCode = codeEnum.getCode();
        this.errorMsg = codeEnum.getDescription();
    }

    public BusinessException() {
    }

    public BusinessException(Integer errorCode, String errorMsg) {
        super(errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinessException(String message) {
        super(message);
    }

    public Integer getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public BusinessException(Throwable cause) {
        super(cause);
    }
}
