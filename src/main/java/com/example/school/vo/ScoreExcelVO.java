package com.example.school.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JSONType(orders = {"clas","name","age","chemistry","mach","physics","biology","english","chinese"})
public class ScoreExcelVO {

    @TableField(value = "class")
    private String clas;

    private String name;

    private Integer age;

    private Integer chemistry;

    private Integer mach;

    private Integer physics;

    private Integer biology;

    private Integer english;

    private Integer chinese;

    //private Integer score;
}
