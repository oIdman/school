package com.example.school.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScoreSelectVO {

    private String clas;

    private String name;

    private Integer age;

    private String subject;

    private Integer score;
}
