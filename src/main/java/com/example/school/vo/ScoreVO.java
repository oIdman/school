package com.example.school.vo;

import com.alibaba.fastjson.annotation.JSONType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JSONType(orders = {"id","name","subject","score"})
public class ScoreVO {
    private Integer id;

    private String name;

    private String subject;

    private Integer score;
}
