package com.example.school.vo;

import com.alibaba.fastjson.annotation.JSONType;
import com.example.school.entity.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JSONType(orders = {"count","teacherList",})
public class TeacherCountVO {

    private Integer count;

    private List<Teacher> teacherList;
}
