package com.example.school.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

/**
 * <p>
 *   课程
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("course")
public class Course  {
    @TableId(
            value = "c_id",
            type = IdType.AUTO
    )
    private Integer c_id;

    /**
     * 课程名称
     */
    private String c_name;

    /**
     * 教师ID
     */
    @NonNull
    private Integer t_id;


}
