package com.example.school.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *  分数
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("score")
public class Score implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 学生ID
     */
    private Integer s_id;

    /**
     * 课程ID
     */
    private Integer c_id;


    /**
     * 分数
     */
    private Integer score;


}
