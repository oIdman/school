package com.example.school.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;
/**
 * <p>
 *  学生成绩excel表
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-21
 */
@Setter
@Getter
public class ScoreExcel {

    @ExcelProperty({"班级"})
    private String clas;

    @ExcelProperty({"姓名"})
    private String name;

//    @ExcelProperty({"年龄"})
//    private Integer age;

    @ExcelProperty({"语文"})
    private Integer chinese;

    @ExcelProperty({"数学"})
    private Integer mach;

    @ExcelProperty({"英语"})
    private Integer english;

    @ExcelProperty({"物理"})
    private Integer physics;

    @ExcelProperty({"化学"})
    private Integer chemistry;

    @ExcelProperty({"生物"})
    private Integer biology;

    @ExcelProperty({"总分"})
    private Integer score;
}
