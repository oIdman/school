package com.example.school.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import lombok.*;

/**
 * <p>
 *  教师
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
@TableName("teacher")
public class Teacher  {


      @TableId(value = "t_id", type = IdType.AUTO)
    private Integer tId;

    /**
     * 姓名
     */
    private String name;


}
