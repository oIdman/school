package com.example.school.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

/**
 * <p>
 *   学生
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor

@TableName("student")
public class Student {
    @TableId(
            value = "id",
            type = IdType.AUTO
    )
    private Integer id;

    private String name;

    private String gender;

    private Integer age;
}
