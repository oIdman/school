package com.example.school.service;

import com.example.school.pojo.ResultJson;
import com.example.school.entity.Student;

import java.util.List;

/**
 * <p>
 *  学生 服务类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface IStudentService {

    List<Student> studentList();

    ResultJson add(Student student);

    ResultJson update(Student student);

    ResultJson delete(Integer id);
}
