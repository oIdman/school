package com.example.school.service.bussiness;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.school.dao.ScoreDao;
import com.example.school.dao.StudentDao;
import com.example.school.dto.ScoreExcelDTO;
import com.example.school.entity.ScoreExcel;
import com.example.school.entity.Student;
import com.example.school.vo.ScoreSelectVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class IScoreExcelService {
    @Autowired
    ScoreDao scoreDao;
    @Autowired
    StudentDao studentDao;

    public void score(OutputStream outputStream, ScoreExcelDTO scoreExcelDTO) {
        //excel表建立开始
        try (ExcelWriter excelWriter = EasyExcel.write(outputStream, ScoreExcel.class).excelType(ExcelTypeEnum.XLSX).build()) {
            //获取未分类的成绩
            List<ScoreSelectVO> scoreSelectVOs = scoreDao.selectScore();
            QueryWrapper<Student> wrapper = new QueryWrapper<>();
            //获取学生列表
            List<Student> studentList = studentDao.selectList(wrapper);
            //获取学生人数
            long count = studentList.stream().count();
            //对成绩按学生姓名进行分类
            Map<String, List<ScoreSelectVO>> map = scoreSelectVOs.stream().collect(Collectors.groupingBy(ScoreSelectVO::getName));
            String[] sKeys = map.keySet().toArray(new String[]{});
            Arrays.sort(sKeys);
            ScoreExcel exl = new ScoreExcel();
            for(String k:sKeys){
                List<Integer> intList = new ArrayList<>();
                exl.setName(k);
                //按科目进行赋值
                for(ScoreSelectVO scoreSelectVO:map.get(k)){
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("语文")){
                        exl.setChinese(scoreSelectVO.getScore());
                    }
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("数学")){
                        exl.setMach(scoreSelectVO.getScore());
                    }
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("英语")){
                        exl.setEnglish(scoreSelectVO.getScore());
                    }
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("物理")){
                        exl.setPhysics(scoreSelectVO.getScore());
                    }
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("化学")){
                        exl.setChemistry(scoreSelectVO.getScore());
                    }
                    if(null != scoreSelectVO.getSubject() && scoreSelectVO.getSubject().equals("生物")){
                        exl.setBiology(scoreSelectVO.getScore());
                    }
                    exl.setClas(scoreSelectVO.getClas());
                    intList.add(scoreSelectVO.getScore());
                }
                //计算总分
                exl.setScore(intList.stream().reduce(Integer::sum).orElse(0));
                //保存每位学生的成绩
                List<ScoreExcel> scoreExcelList = new ArrayList<>();
                scoreExcelList.add(exl);
                //终止条件
                if(map.size() == count){
                    //输出excel表
                    WriteSheet writeSheet = EasyExcel.writerSheet("学生成绩表").build();
                    excelWriter.write(scoreExcelList, writeSheet);
                }
            }
        }
    }
}

