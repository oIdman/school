package com.example.school.service;

import com.example.school.dto.TeacherDTO;
import com.example.school.entity.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.school.pojo.ResultJson;

/**
 * <p>
 *  教师 服务类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface ITeacherService extends IService<Teacher> {

    void addTeacher(String name);
}
