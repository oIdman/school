package com.example.school.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.school.dto.ScoreDTO;
import com.example.school.entity.Score;
import com.example.school.dao.ScoreDao;
import com.example.school.pojo.ResultJson;
import com.example.school.service.IScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  分数 服务实现类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreDao, Score> implements IScoreService {

    @Autowired
    private ScoreDao scoredao;

    @Override
    public List<Score> scoreList() {
        List<Score> scores = scoredao.selectList(new QueryWrapper<>());
        return scores;
    }

    @Override
    public ResultJson scoreAdd(ScoreDTO scoreDTO) {
        Score score = new Score();
        BeanUtils.copyProperties(scoreDTO,score);
        int result = scoredao.insert(score);
        return result == 1 ? ResultJson.success("请求成功"):ResultJson.fail("请求失败");
    }
}
