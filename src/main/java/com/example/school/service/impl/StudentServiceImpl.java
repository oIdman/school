package com.example.school.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.school.dao.StudentDao;
import com.example.school.entity.Student;
import com.example.school.pojo.ResultJson;
import com.example.school.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  学生 服务实现类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements IStudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Student> studentList() {
        return studentDao.selectAll();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultJson add(Student student) {
        int result = studentDao.insert(student);
        return result == 1 ? ResultJson.success("请求成功"):ResultJson.fail("请求失败");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultJson update(Student student) {
        Student student1 = studentDao.selectOne(new QueryWrapper<Student>()
                .eq("name",student.getName()));
        student.setId(student1.getId());
        int result = studentDao.updateById(student);
        return result == 1 ? ResultJson.success("请求成功") : ResultJson.fail("请求失败");
    }

    @Override
    public ResultJson delete(Integer id){
        int result = studentDao.deleteById(id);
        return result == 1 ? ResultJson.success("请求成功") : ResultJson.fail("请求失败");
    }

}
