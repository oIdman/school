package com.example.school.service.impl;

import com.example.school.dao.CourseDao;
import com.example.school.dto.CourseDTO;
import com.example.school.entity.Course;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.school.pojo.ResultJson;
import com.example.school.service.ICourseService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  课程 服务实现类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Service
public class CourseServiceImpl extends ServiceImpl<CourseDao, Course> implements ICourseService {
    @Autowired
    private CourseDao courseDao;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResultJson addCourse(Course course) {
//        Course course = new Course();
//        course.setTId(tId);
//        course.setCName(cName);
        int result = courseDao.insert(course);

        return result == 1 ? ResultJson.success("请求成功"):ResultJson.fail("请求失败");
    }
}
