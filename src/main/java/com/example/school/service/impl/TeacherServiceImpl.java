package com.example.school.service.impl;

import com.baomidou.mybatisplus.core.assist.ISqlRunner;
import com.example.school.dto.TeacherDTO;
import com.example.school.entity.Score;
import com.example.school.entity.Teacher;
import com.example.school.dao.TeacherDao;
import com.example.school.pojo.ResultJson;
import com.example.school.service.ITeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  教师 服务实现类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherDao, Teacher> implements ITeacherService {

    @Autowired
    private TeacherDao teacherdao;

    @Override
    public void addTeacher(String name) {
        //teacherdao.addTeacher(name);
        Teacher teacher = new Teacher();
        teacher.setName(name);
        teacherdao.insert(teacher);
    }

}
