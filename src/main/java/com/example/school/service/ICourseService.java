package com.example.school.service;

import com.example.school.dto.CourseDTO;
import com.example.school.entity.Course;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.school.pojo.ResultJson;

/**
 * <p>
 *  课程 服务类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface ICourseService extends IService<Course> {

    ResultJson addCourse(Course course);
}
