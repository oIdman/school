package com.example.school.service;

import com.example.school.dto.ScoreDTO;
import com.example.school.entity.Score;
import com.example.school.pojo.ResultJson;

import java.util.List;

/**
 * <p>
 *  分数 服务类
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface IScoreService {

    List<Score> scoreList();

    ResultJson scoreAdd(ScoreDTO scoreDTO);

}
