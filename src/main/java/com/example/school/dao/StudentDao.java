package com.example.school.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.school.entity.Student;

import java.util.List;
/**
 * <p>
 *  Mapper 学生
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface StudentDao extends BaseMapper<Student> {

    List<Student> selectAll();
}
