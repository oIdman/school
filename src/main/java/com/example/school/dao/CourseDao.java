package com.example.school.dao;

import com.example.school.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 课程
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface CourseDao extends BaseMapper<Course> {

}
