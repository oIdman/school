package com.example.school.dao;

import com.example.school.entity.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 教师
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface TeacherDao extends BaseMapper<Teacher> {
    void addTeacher(String name);
}
