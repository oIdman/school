package com.example.school.dao;

import com.example.school.entity.Score;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.school.vo.ScoreExcelVO;
import com.example.school.vo.ScoreSelectVO;

import java.util.List;

/**
 * <p>
 *  Mapper 分数
 * </p>
 *
 * @author chenshijie
 * @since 2022-09-20
 */
public interface ScoreDao extends BaseMapper<Score> {

    List<ScoreSelectVO> selectScore();
}
