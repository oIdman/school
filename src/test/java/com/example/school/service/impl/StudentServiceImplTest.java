package com.example.school.service.impl;

import com.example.school.entity.Student;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class StudentServiceImplTest {

    @Autowired
    private StudentServiceImpl studentServiceImpl;

    @BeforeEach
    void setUp() {
    }

    @Test
    void studentList() {
        List<Student> studentList = studentServiceImpl.studentList();
//        Student student = new Student();
        for (Student student:studentList) {
            System.out.println(student);
        }
    }

    @Test
    void add() {
        Student student = new Student();
        student.setName("王二");
        student.setAge(18);
        student.setGender("女");
        studentServiceImpl.add(student);

    }

    @Test
    void update() {
        Student student = new Student();
        student.setName("李一");
        student.setAge(12);
        student.setGender("女");
        studentServiceImpl.update(student);
    }

    @Test
    void delete(){
        studentServiceImpl.delete(2);
    }
}